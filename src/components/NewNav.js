import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import "../_variables.scss";

let toggle;
let list;

class NewNav extends Component {
    constructor(props) {
        super(props);

        this.state = {
            animationClass: false
        }
    }

    componentDidMount() {
        toggle = document.querySelector('.nav-toggle');
        list = document.querySelector('.nav-list');
    }

    triggerClass(element, className) {
        void element.offsetWidth;
        element.classList.toggle(className);
    }

    shouldComponentUpdate(nextProps, nextState) {
        if(this.state.animationClass !== nextState.animationClass){
            this.triggerClass(toggle, 'active');
            this.triggerClass(list, 'show');
        }
        return false;
    }

    handleNavMobileClick = (e) => {
        let mNavClick = !this.state.animationClass;
        this.setState({animationClass: mNavClick})
    }

    render() {
        const Nav = styled.nav`
            float: right;
            @media only screen and (max-width: 799px) {
                float: none;
            }
            ul {
                list-style: none;
                margin: 0;
                padding: 0;
                @media only screen and (max-width: 799px) {
                    flex-direction: column;
                    display: none;
                }
                a {
                    float: left;
                    position: relative;
                    li {
                        display: block;
                        padding: 0 20px;
                        line-height: 70px;
                        background: #262626;
                        color: #FFFFFF;
                        text-decoration: none;

                        &:hover {
                            background: #2581DC;
                            color: #FFFFFF;
                        }
                        &:not(:only-child):after {
                            padding-left: 4px;
                            content: ' ▾';
                        }
                    }
                    .nav-dropdown {
                        position: absolute;
                        display: none;
                        z-index: 1;
                        box-shadow: 0 3px 12px rgba(0, 0, 0, 0.15);
                    }
                    ul li {
                        min-width: 190px;
                        a{
                            padding: 15px;
                            line-height: 20px;
                        }
                    }
                }
            }
            .show {display: flex;}
        `;

        const NavMobile = styled.div`
            display: none;
            position: absolute;
            top: 0;
            right: 0;
            background: #262626;
            height: 70px;
            width: 70px;

            @media only screen and (max-width: 799px) {
                display: block;
                z-index: 1;
            }

            .nav-toggle {
                position: absolute;
                left: 18px;
                top: 22px;
                cursor: pointer;
                padding: 10px 35px 16px 0px;

                span,
                span:before,
                span:after {
                    position: absolute;
                    display: block;
                    content: '';
                    cursor: pointer;
                    border-radius: 1px;
                    height: 5px;
                    width: 35px;
                    background: #FFFFFF;
                    transition: all 300ms ease-in-out;
                }
                span:before {
                    top: -10px;
                }
                span:after {
                    bottom: -10px;
                }
                &.active span {
                    background-color: transparent;
                    &:before,
                    &:after {
                        top: 0;
                    }
                    &:before {
                        transform: rotate(45deg);
                    }
                    &:after {
                        transform: rotate(-45deg);
                    }
                }
            }
        `;
        
        let navItems = this.props.list.map(function (item, index) {
            return <Link to={item.path} key={index}><li>{item.title}</li></Link>
        });

        return (
            <Nav>
                <NavMobile>
                    <a className="nav-toggle" href="#!" onClick={this.handleNavMobileClick}><span></span></a>
                </NavMobile>
                <ul className="nav-list">
                    {navItems}
                    {/* <li><a href="#!">Services</a>
                        <ul className="nav-dropdown">
                            <li><a href="#!">Web Design</a></li>
                            <li><a href="#!">Web Development</a></li>
                            <li><a href="#!">Graphic Design</a></li>
                        </ul>
                    </li> */}
                </ul>
            </Nav>
        )
    }
}

export default NewNav;