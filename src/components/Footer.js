import React from 'react'; // eslint-disable-line no-unused-vars
import styled from 'styled-components';

const FooterStyles = styled.footer`
    display: block;
    width: 100%;
    height: ${props => props.height? props.height : 100}px;
    background: ${props => props.color? props.color : '#4a4a4a'};
    border-top: 2px #C5B941 solid;
`;

const Footer = (props) => {
    const { height, color } = props;

    return  (
        <FooterStyles height={height} color={color}>
            {props.children}
        </FooterStyles>
    )
}

export default Footer;