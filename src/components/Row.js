import React from 'react'; // eslint-disable-line no-unused-vars
import styled from 'styled-components';

const Row = styled.div`
    &::after {
        content: "";
        clear: both;
        display: table;
    }
`;

export default Row;