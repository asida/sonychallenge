import React from 'react'; // eslint-disable-line no-unused-vars
import styled from 'styled-components';

const HeaderStyles = styled.header`
    width: 100%;
    height: ${props => props.height? props.height : 60}px;
    background: ${props => props.color? props.color : '#4a4a4a'};
`;

const Header = (props) => {
    const { height, color } = props;

    return  (
        <HeaderStyles height={height} color={color}>
            {props.children}
        </HeaderStyles>
    )
}

export default Header;