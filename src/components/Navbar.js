import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';


class Navbar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            mobileNavHidden: true
        }
    }

    handleIconNavClick= () => {
        let iconState = !this.state.mobileNavHidden;
        this.setState({mobileNavHidden: iconState});
    }

    render() {
        const UlWrapperStyle = styled.div`
            position: absolute;
            width: 400px;
            right: 0;
            bottom: 0;
            @media only screen and (max-width: 668px) {
                position: relative;
                width: 100%;
                top: 80px;
                background: #4a4a4a;
                border-bottom: 2px #C5B941 solid;
            }
        `;

        const NavbarStyles = styled.ul`
            display: flex;
            list-style: none;
            margin: 0;
            justify-content: space-evenly;
            flex-direction: ${props => props.direction? props.direction : 'row'};
            li.icon { display: none; }

            @media only screen and (max-width: 668px) {
                flex-direction: column;

                li { 
                    display: ${this.state.mobileNavHidden? 'none' : 'block' };
                }

                li.icon {
                    display: block;
                    position: absolute;
                    top: 0;
                    right: 0;
                    margin: -45px 20px;
                }
            }
        `;

        const LiStyled = styled.li`
            color: #FFFFFF;
            font-family: fantasy;
            font-size: 18px;
            &:hover {
                color: #C5B941;
            }
        `;

        const { list, direction } = this.props;
        let navItems = list.map(function (item, index) {
            return <Link to={item.path} key={index}><LiStyled>{item.title}</LiStyled></Link>
        });

        return (
            <UlWrapperStyle>
                <NavbarStyles direction={direction}>
                    {navItems}
                    <li className="icon" onClick={this.handleIconNavClick}>X</li>
                </NavbarStyles>
            </UlWrapperStyle>
        )
    }
}

export default Navbar;