import React from 'react'; // eslint-disable-line no-unused-vars

const Card = (props) => {
    const { title } = props;

    return  (
        <div style={{boxShadow: '3px 8px 10px #3c38385e'}} className="ui card">
            <div className="content">
                <div className="header">{title}</div>
            </div>
            <div className="content">
                <div className="ui feed">
                    {props.children}
                </div>
            </div>
        </div>
    )
}

export default Card;