const Employee = {
    "info": {
        "firstname": "John",
        "lastname": "Snow",
        "age": 31,
        "title": "Software Engineer",
        "yearsCompleted": 2
    },
    "benefits": ['Unlimited PTO', '401K Match', 'Medical/Dental Insurance', 'Commuter benefits'],
    "timeOff": {
        "pto": 60,
        "sick": 40,
        "personal": 16
    },
    "payStubs": [{
        "date": "01/21/2017",
        "total": "$3,000.01"
    },{
        "date": "02/6/2017",
        "total": "$3,010.67"
    },{
        "date": "03/25/2017",
        "total": "$2,970.97"
    },{
        "date": "04/5/2017",
        "total": "$3,100.23"
    }],
    "career": {
        "jobs": ["Software Test Engineer @ Snapchat", "Software Engineer @ Apple"],
        "experience": "Full Stack Developer with experience in multiple industries and technologies.",
        "resume": "MyResume.pdf"
    }
}

export { Employee };