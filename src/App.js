import React from 'react'; // eslint-disable-line no-unused-vars
import './_variables.scss';
import Home from './containers/Home';
import Dashboard from './containers/Dashboard';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

const NotFound = () => {
  return (
    <h1>Route not found</h1>
  )
}

const App = () => (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/signin" component={Home} />
        <Route path="/dashboard" component={Dashboard} />
        <Route path="*" component={NotFound} />
      </Switch>
    </Router>
)

export default App;