import React, {Component} from 'react'; // eslint-disable-line no-unused-vars
import { withRouter } from 'react-router-dom';

class Home extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
        let values = document.querySelectorAll('.field input');
        if(values[0].value && values[1].value){
            localStorage.setItem('authenticated', true);
            localStorage.setItem('user', values[0].value);
            this.props.history.push('/dashboard');
        }
    }

    render() {
        return(
            <div className="ui one column stackable center aligned page grid">
                <div className="column twelve wide">
                    <form className="ui form">
                        <div className="field">
                            <label>Username</label>
                            <input placeholder="Username" />
                        </div>
                        <div className="field">
                            <label>Password</label>
                            <input type="password" placeholder="Password" />
                        </div>

                        <button className="ui button" onClick={this.handleSubmit}>
                            Login
                        </button>
                    </form>
                </div>
            </div>
        )
    }
}

export default withRouter(Home);