import React, {Component} from 'react'; // eslint-disable-line no-unused-vars
import {Employee} from '../data/Employee';
import Card from '../components/Card';
import Header from '../components/Header';
import { Link } from 'react-router-dom';

let content;
let employeeInfo;
let benefitsInfo;
let timeOffInfo;
let payInfo;
let careerInfo;

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.isSignedIn = localStorage.getItem('authenticated');
    }
    
    renderEmployeeInfoMarkup = (data) => {
        return (
            <div>
                <ul>
                    <li><span style={{fontWeight: 'bold'}}>Name:</span> {data.firstname} {data.lastname}</li>
                    <li><span style={{fontWeight: 'bold'}}>Age:</span> {data.age}</li>
                    <li><span style={{fontWeight: 'bold'}}>Title:</span> {data.title}</li>
                    <li><span style={{fontWeight: 'bold'}}>Years Completed:</span> {data.yearsCompleted}</li>
                </ul>
            </div>
        )
    }

    renderBenefitsInfoMarkup = (data) => {
        let benefits = data.map(function(b, index){
            return (<li key={index}>{b}</li>)
        });

        return <div><ul>{benefits}</ul></div>;
    }

    renderTimeOffInfoMarkup = (data) => {
        return (
            <div>
                <ul>
                    <li><span style={{fontWeight: 'bold'}}>PTO:</span> {data.pto}</li>
                    <li><span style={{fontWeight: 'bold'}}>Sick:</span> {data.sick}</li>
                    <li><span style={{fontWeight: 'bold'}}>Personal:</span> {data.personal}</li>
                </ul>
            </div>
        )
    }

    renderPayInfoMarkup = (data) => {
        let checks = data.map(function(b, index){
            return (<li key={index}><a href="">{b.date} - {b.total}</a></li>)
        });

        return <div><ul>{checks}</ul></div>;
    }

    renderCareerInfoMarkup = (data) => {
        return (
            <div>
                <ul>
                    <li><span style={{fontWeight: 'bold'}}>Experience:</span> {data.experience}</li>
                    <li><span style={{fontWeight: 'bold'}}>Jobs:</span> {data.jobs.join(', ')}</li>
                    <li><span style={{fontWeight: 'bold'}}>Resume:</span> <a href="">{data.resume}</a></li>
                </ul>
            </div>
        )
    }

    handleLogout = () => {
        if(this.isSignedIn) {
            localStorage.removeItem('authenticated');
            localStorage.removeItem('user');
        }
    }

    render() {
        if(this.isSignedIn) {
            // will use fetched data from API
            employeeInfo = this.renderEmployeeInfoMarkup(Employee.info);
            benefitsInfo = this.renderBenefitsInfoMarkup(Employee.benefits);
            timeOffInfo = this.renderTimeOffInfoMarkup(Employee.timeOff);
            payInfo = this.renderPayInfoMarkup(Employee.payStubs);
            careerInfo = this.renderCareerInfoMarkup(Employee.career);
        } else {
            content = (
                <div>
                    <img style={{maxWidth:'100%'}} src="https://react.semantic-ui.com/assets/images/wireframe/paragraph.png" alt="no data to show"/>
                </div>
            )
        }

        return (
            <div style={{margin: '0 auto'}} className="ui vertically centered grid fluid">
                <div style={{padding: 0, textAlign: 'left'}} className="row stretched">
                    <Header color="cornflowerblue">
                        <div>
                            <h2 style={{position:'absolute',bottom: 0,padding:'0 53px 5px',color:'#FFFFFF',textShadow:'1px 1px 0px black'}}>{(this.isSignedIn)? `Welcome ${localStorage.getItem('user')}!` : ""}</h2>
                        </div>
                    </Header>
                </div>
                <div className="three column row">
                    <div className="five wide column">
                        <Card title='Employee Info'>
                            {(this.isSignedIn)? employeeInfo : content}
                        </Card>
                    </div>
                    <div className="five wide column">
                        <Card title='Benefits'>
                            {(this.isSignedIn)? benefitsInfo : content}
                        </Card>
                    </div>
                    <div className="five wide column">
                        <Card title='Time Off'>
                            {(this.isSignedIn)? timeOffInfo : content}
                        </Card>
                    </div>
                </div>
                <div className="three column row">
                    <div className="five wide column">
                        <Card title='Pay Stubs'>
                            {(this.isSignedIn)? payInfo : content}
                        </Card>
                    </div>
                    <div className="five wide column">
                        <Card title='Career'>
                            {(this.isSignedIn)? careerInfo : content}
                        </Card>
                    </div>
                    <div className="five wide column">
                        <Card title={(this.isSignedIn)? 'Logout' : 'Sign In to view your information!'}>
                            <Link style={{textAlign: 'center'}} to={(this.isSignedIn)? "/" : "/signin"} onClick={this.handleLogout}>
                                <i aria-hidden="true" className={`${(this.isSignedIn)? "sign out" : "sign in"} huge icon`}></i>
                            </Link>
                        </Card>
                    </div>
                </div>
            </div>
        )
    }
}

export default Dashboard;